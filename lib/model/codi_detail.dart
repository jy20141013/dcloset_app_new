class CodiDetail {
  num id;
  String imgUrl;
  String itemName;
  num itemPrice;
  String detailImgUrl1;
  String detailImgUrl2;
  String detailImgUrl3;

  CodiDetail(this.id, this.imgUrl, this.itemName, this.itemPrice, this.detailImgUrl1, this.detailImgUrl2, this.detailImgUrl3);
}