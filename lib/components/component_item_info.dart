import 'package:dclost_app/model/codi_detail.dart';
import 'package:flutter/material.dart';

// 제품 리스트 보여주기 폼

class ComponentItemInfo extends StatelessWidget {
  const ComponentItemInfo({
    super.key,
    required this.codiDetail,
    required this.callback
  });

  final CodiDetail codiDetail;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              height: 100,
              child: Image.asset(
                  codiDetail.imgUrl,
              fit: BoxFit.fitHeight,),
            ),
            Container(
              child: Text(codiDetail.itemName),
            ),
            Container(
              child: Text(
                  '${codiDetail.itemPrice}원',
              style: TextStyle(
                fontSize: 10
              ),),
            ),
          ],
        ),
      ),
    );
  }
}
