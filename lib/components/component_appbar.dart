import 'package:flutter/material.dart';

class ComponentAppbar extends StatelessWidget implements PreferredSizeWidget {
  const ComponentAppbar({super.key});

  @override
  Widget build(BuildContext context) {
    // 앱바 앱 내 공통 적용, 단 제품 상세페이지의 경우 앱바 수정하여 뒤로가기 버튼 만들기
    return AppBar(
      backgroundColor: Color(0xFFFFFF),
      title: Container(
        height: 60,
        alignment: Alignment.center,
        child: Image.asset('assets/logoicon.png'),
      ),
      actions: [
        IconButton(
          onPressed: (){},
          icon: Icon(Icons.account_circle_outlined),
        )
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(60);
}
