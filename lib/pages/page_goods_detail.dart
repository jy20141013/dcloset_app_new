import 'package:dclost_app/model/codi_detail.dart';
import 'package:flutter/material.dart';
class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({
    super.key,
    required this.codiDetail
  });

  final CodiDetail codiDetail;

  // 제품 상세보기 페이지
  // 하단 주문하기 버튼 만들어야 함. ( 바텀네비바? 고정 버튼? 드로어?.. )
  // 제품 상세 정보 내용 좀더 그럴싸하게 디자인해야 함.

  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Image.asset(widget.codiDetail.imgUrl),
                  Text(
                      widget.codiDetail.itemName
                  ),
                  Text(
                      '${widget.codiDetail.itemPrice}원'
                  )
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Image.asset(widget.codiDetail.detailImgUrl1),
                  Image.asset(widget.codiDetail.detailImgUrl2),
                  Image.asset(widget.codiDetail.detailImgUrl3)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
